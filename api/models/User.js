/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    id:
    {
        type: 'string',
        columnName: '_id'
    },
    player:
    {
        type: 'number',
        unique: true,
        autoIncrement: true
    },
    exp: 'number',
    games: 'number',
    victories: 'number',
    defeats: 'number',
    nick:
    {
        type: 'string',
        unique: true
    },
    password: 'string',
    email:
    {
        type: 'string',
        unique: true,
        isEmail: true,
    },
    active: 'boolean',
    LastLogin: { type: 'number', autoUpdatedAt: true, },

  },

};
