/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const shortid = require('shortid');


module.exports = {
    'panel': function(req, res)
    {

    },

    'login': async function(req, res)
    {
        let loggingUser = await User.find({
             email:
             {
                'contains': req.body.email
             },
             password:
             {
                 'contains': req.body.password
             }
        });
        if (loggingUser.length === 1)
        {
            let startDate = new Date();
            let endDate = new Date(startDate.getTime() + (15 * 60000));
            req.session._expires = endDate;
            req.session.authenticated = true;
            req.session.userId = loggingUser[0].id;
            res.view('pages/homepage.ejs');
        }
        else
        {
            res.view('user/loginErr');
        }
    },
    'logout': async function(req, res)
    {
        req.session.authenticated = false;
        req.session.destroy();
        res.view('pages/homepage.ejs');
    },
    'create': function(req, res)
    {
        res.view();
    },

    'signup': async function (req, res, next)
    {
        let createdUser = await User.create(
        {
            exp: 0,
            Victories: 0,
            Dafeats:0,
            nick: req.body.nick,
            password: req.body.password1,
            email: req.body.email,
            active: false
        }).exec(function(err, user)
        {
            if (err)
                console.log(err);
            if (user)
                console.log(user);

        });


        //------------------------------------------------------------

        let code = shortid.generate();
        let title = "Activation code";
        let text = `To activate your account, please visit the localhost:1337/user/active website and enter this code - ${code}.`;

        let createdCode = await Code.create(
        {
            code: code,
            email: req.body.email,
        }).exec(async function(err)
        {
            if (err)
                console.log(err);


            let send = await sails.helpers.email(req.body.email, title, text);
            console.log(send);


        });
        res.view();
    },

    'active': function(req, res)
    {
        res.view();
    },
    'activeErr': function(req, res)
    {
        res.view();
    },
    'activated': function(req, res)
    {

        Code.findOne(
        {
            code: req.body.code
        }).exec(function (err1, code)
        {
            if (err1)
                console.log(err1);
            if (code)
            {
                console.log(code);
                res.view();
                User.updateOne({ email: code.email }).set({ active: true }).exec(function (err2, result)
                {
                    if(err2)
                        console.log(err2);
                    if (result)
                    {
                        console.log(result);
                        Code.destroyOne(code).exec(function (err3)
                        {
                            if (err3)
                                console.log(err3);

                        });
                    }
                });
            }
            else
                res.view('/user/activeErr');
        });
    },
};
