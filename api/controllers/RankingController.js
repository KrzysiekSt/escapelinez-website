/**
 * RankingController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    'list': async function(req, res)
    {
        let total = await User.count({active: true});
        res.send(total);
    },

    'data': async function(req, res)
    {
        let records = await User.find({where: { active: true }, limit: 50, sort: 'exp DESC'});
        res.send(records);
    }

};
