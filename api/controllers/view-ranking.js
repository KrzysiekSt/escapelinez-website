module.exports = {


  friendlyName: 'View ranking',


  description: 'Display "Ranking" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/ranking'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
