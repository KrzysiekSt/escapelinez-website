const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport(
{
    service: 'gmail',
    auth:
    {
        user: 'greenlightprogramminginfo@gmail.com',
        pass: 'z9ukBXNbAuzfCBc'
    }
});

module.exports = {


  friendlyName: 'Email',


  description: 'Email Sending.',


  inputs:
  {
      email:
      {
          type: 'ref',
          required: true
      },
      title:
      {
          type: 'string',
          required: true
      },
      text:
      {
          type: 'string',
          required: true
      },
  },


exits:
{
    success:
    {
        description: 'All done.',
    },
},


    fn: async function (inputs, exits)
    {
        let mailOptions =
        {
            from: 'greenlightprogramminginfo@gmail.com',
            to: inputs.email,
            subject: inputs.title,
            text: inputs.text
        };
        await transporter.sendMail(mailOptions, function(err, info)
        {
            if (err)
            {
                console.log(err);
            }
            else
            {
                console.log('Email sent: ' + info.response);
            }
        });
        return exits.success.description;
    }


};
