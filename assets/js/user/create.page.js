$('#SignUp').parsley(
    {
        errorClass: 'is-invalid',
        successClass: 'is-valid',
        uiEnabled: true,
        errorTemplate: '',

    }
);

document.getElementById('InputEmail1').addEventListener('change', function()
{
    $('#InputEmail1').parsley().validate();
});

document.getElementById('InputNick').addEventListener('change', function()
{
    $('#InputNick').parsley().validate();
});

document.getElementById('InputPassword1').addEventListener('change', function()
{
    $('#InputPassword1').parsley().validate();
});

document.getElementById('InputPassword2').addEventListener('change', function()
{
    $('#InputPassword2').parsley().validate();
});

document.getElementById('send').addEventListener('click', function()
{
    $('#SignUp').parsley().validate()
});
